import httpStatus from 'http-status-codes';

import { MissingParamError } from '@shared/errors';

import IHttpRequest from '../interfaces/IHttpRequest';
import IHttpResponse from '../interfaces/IHttpResponse';

export default abstract class BaseController {
  protected formatHttpErrorResponse(
    statusCode: number,
    error: Error,
  ): IHttpResponse {
    return {
      statusCode,
      body: error.message,
    };
  }

  protected formatHttpSuccessResponse<T = any>(
    statusCode: number,
    payload: T,
  ): IHttpResponse {
    return {
      statusCode,
      body: payload,
    };
  }

  protected handleMissingParams(
    params: string[],
    httpRequest: IHttpRequest,
    requestKey = 'body',
  ): void {
    for (let i = 0; i < params.length; i += 1) {
      if (!httpRequest[requestKey as keyof IHttpRequest][params[i]]) {
        throw new MissingParamError(params[i], httpStatus.BAD_REQUEST);
      }
    }
  }
}
