export default class MissingParamError extends Error {
  constructor(paramName: string, public status: number) {
    super(`You should provide the required param: ${paramName}`);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
