import moduleAlias from 'module-alias';
import path from 'path';

const files = path.resolve(__dirname, '..', '..');

moduleAlias.addAliases({
  '@src': path.join(files, 'src'),
  '@tests': path.join(files, '__tests__'),
  '@config': path.join(files, 'src', 'config'),
  '@controllers': path.join(files, 'src', 'controllers'),
  '@middlewares': path.join(files, 'src', 'middlewares'),
  '@routes': path.join(files, 'src', 'routes'),
  '@services': path.join(files, 'src', 'services'),
  '@shared': path.join(files, 'src', 'shared'),
  '@interfaces': path.join(files, 'src', 'interfaces'),
});
