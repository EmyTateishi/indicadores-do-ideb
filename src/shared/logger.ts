import pino from 'pino';

import { loggerConfig } from '@config/index';

export default pino({
  enabled: loggerConfig.enabled,
  level: loggerConfig.level,
});
