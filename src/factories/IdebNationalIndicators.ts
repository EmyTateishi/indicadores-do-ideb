import { IdebNationalIndicatorsController } from '@src/controllers';
import { ICacheService, IController } from '@src/interfaces';
import IdebScrapperService from '@src/services/IdebScrapperService';

import { IdebScrapperConfig } from '@config/index';

export default (cacheService: ICacheService): IController => {
  const idepScraperService = new IdebScrapperService(IdebScrapperConfig);
  return new IdebNationalIndicatorsController(idepScraperService, cacheService);
};
