import './shared/moduleAlias';

import { logger } from '@shared/index';

import { appConfig } from '@config/index';

import { ExitStatus } from '@interfaces/enums';

import App from './App';
import idepScrapperConfig from './config/IdebScrapperConfig';
import IdebScrapperService from './services/IdebScrapperService';

process.on('unhandledRejection', (reason, promise) => {
  logger.error(
    `App exiting due to an unhandled promise: ${promise} and reason: ${reason}`,
  );

  throw reason;
});

process.on('uncaughtException', error => {
  logger.error(`App exiting due to an unhandled exception: ${error}`);

  process.exit(ExitStatus.FAILURE);
});

(async () => {
  try {
    const app = new App(appConfig.PORT);
    app.initServer();
    const exitSignals: NodeJS.Signals[] = ['SIGINT', 'SIGTERM', 'SIGQUIT'];
    exitSignals.forEach((signal: string) => {
      process.on(signal, (): void => {
        try {
          logger.info('App exited with success');
          process.exit(ExitStatus.SUCCESS);
        } catch (error) {
          logger.info('App exited with error');
          process.exit(ExitStatus.FAILURE);
        }
      });
    });
  } catch (error) {
    logger.error(`App exited with error: ${error}`);
    logger.error(error);
    process.exit(ExitStatus.FAILURE);
  }
})();
