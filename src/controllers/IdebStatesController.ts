import httpStatus from 'http-status-codes';

import BaseController from '@src/abstracts/AbstractController';
import {
  ICacheService,
  IController,
  IHttpRequest,
  IHttpResponse,
} from '@src/interfaces';
import { CacheKeys } from '@src/interfaces/enums';
import IdebScrapperService from '@src/services/IdebScrapperService';
import { logger } from '@src/shared';

export default class IdebStatesController
  extends BaseController
  implements IController {
  constructor(
    private idebScrapperService: IdebScrapperService,
    private cacheService: ICacheService,
  ) {
    super();
  }

  async handleRoute(httpRequest: IHttpRequest): Promise<IHttpResponse> {
    logger.info('Getting the list of UFs');
    const { nocache } = httpRequest.query;
    const statesFromCache = this.cacheService.get(CacheKeys.STATES);
    logger.info('Checking if the data already exists on cache');
    if (statesFromCache && !nocache) {
      logger.info('Returning the list of UFs from the cache');
      return this.formatHttpSuccessResponse(
        httpStatus.OK,
        JSON.parse(statesFromCache),
      );
    }

    const data = await this.idebScrapperService.getAllUfData();
    this.cacheService.set(CacheKeys.STATES, JSON.stringify(data));
    return this.formatHttpSuccessResponse(httpStatus.OK, data);
  }
}
