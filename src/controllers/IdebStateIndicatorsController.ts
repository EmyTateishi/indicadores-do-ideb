import httpStatus from 'http-status-codes';

import BaseController from '@src/abstracts/AbstractController';
import {
  ICacheService,
  IController,
  IHttpRequest,
  IHttpResponse,
} from '@src/interfaces';
import { CacheKeys } from '@src/interfaces/enums';
import IdebScrapperService from '@src/services/IdebScrapperService';
import { logger } from '@src/shared';

export default class IdebStateController
  extends BaseController
  implements IController {
  constructor(
    private idebScrapperService: IdebScrapperService,
    private cacheService: ICacheService,
  ) {
    super();
  }

  async handleRoute(httpRequest: IHttpRequest): Promise<IHttpResponse> {
    this.handleMissingParams(['uf'], httpRequest, 'query');

    const { city, uf, grade, rede, nocache } = httpRequest.query;
    logger.info(
      `Getting data with the params: uf: ${uf}, grade: ${grade || ''}, rede: ${
        rede || ''
      }`,
    );

    const indicatorsCacheKey = this.normalizeCacheKey({
      uf,
      grade,
      rede,
    });

    const indicatorsFromCache = this.cacheService.get(indicatorsCacheKey);

    if (indicatorsFromCache && !nocache) {
      logger.info(
        `Returning data with the params: city: ${city}, uf: ${uf}, grade: ${
          grade || ''
        }, rede: ${rede || ''} from the cache`,
      );
      return this.formatHttpSuccessResponse(
        httpStatus.OK,
        JSON.parse(indicatorsFromCache),
      );
    }

    const data = await this.idebScrapperService.getIdebDataByUf({
      uf,
      grade,
      rede,
    });

    this.cacheService.set(indicatorsCacheKey, JSON.stringify(data));
    return this.formatHttpSuccessResponse(httpStatus.OK, data);
  }

  private normalizeCacheKey({
    uf,
    grade,
    rede,
  }: {
    uf: string;
    grade?: string;
    rede?: string;
  }): string {
    return `${CacheKeys.INDICATORS}_${uf.toUpperCase()}_${
      grade ? `_${grade.toUpperCase()}` : ''
    }${rede ? `_${rede.toUpperCase()}` : ''}`;
  }
}
