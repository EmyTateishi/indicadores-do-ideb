import httpStatus from 'http-status-codes';

import BaseController from '@src/abstracts/AbstractController';
import {
  ICacheService,
  IController,
  IHttpRequest,
  IHttpResponse,
} from '@src/interfaces';
import IdebScrapperService from '@src/services/IdebScrapperService';
import { logger } from '@src/shared';

export default class IdebNationalController
  extends BaseController
  implements IController {
  constructor(
    private idebScrapperService: IdebScrapperService,
    private cacheService: ICacheService,
  ) {
    super();
  }

  async handleRoute(httpRequest: IHttpRequest): Promise<IHttpResponse> {
    const { nocache, rede } = httpRequest.query;
    logger.info('Getting national data');

    const indicatorsCacheKey = `ideb_national_indicators${
      rede ? `_${rede.toUpperCase()}` : ''
    }`;

    const indicatorsFromCache = this.cacheService.get(indicatorsCacheKey);

    if (indicatorsFromCache && !nocache) {
      logger.info('Returning national data from the cache');
      return this.formatHttpSuccessResponse(
        httpStatus.OK,
        JSON.parse(indicatorsFromCache),
      );
    }

    const data = await this.idebScrapperService.getIdebDataByCountry(rede);

    this.cacheService.set(indicatorsCacheKey, JSON.stringify(data));
    return this.formatHttpSuccessResponse(httpStatus.OK, data);
  }
}
