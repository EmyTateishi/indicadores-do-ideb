import httpStatus from 'http-status-codes';

import BaseController from '@src/abstracts/AbstractController';
import {
  ICacheService,
  IController,
  IHttpRequest,
  IHttpResponse,
} from '@src/interfaces';
import { CacheKeys } from '@src/interfaces/enums';
import IdebScrapperService from '@src/services/IdebScrapperService';
import { logger } from '@src/shared';
import { MissingParamError } from '@src/shared/errors';

export default class IdepCitiesByStateController
  extends BaseController
  implements IController {
  constructor(
    private idebScrapperService: IdebScrapperService,
    private cacheService: ICacheService,
  ) {
    super();
  }

  async handleRoute(httpRequest: IHttpRequest): Promise<IHttpResponse> {
    const { uf } = httpRequest.params;
    const { nocache } = httpRequest.query;
    if (!uf) {
      throw new MissingParamError('uf', httpStatus.BAD_REQUEST);
    }

    logger.info(`Getting the list of cities of the uf [${uf}]`);
    const citiesFromCache = this.cacheService.get(
      `${CacheKeys.CITIES}_${String(uf).toUpperCase()}`,
    );

    logger.info(
      `Checking if the list of cities of the uf [${uf}] exists on cache`,
    );
    if (citiesFromCache && !nocache) {
      logger.info(
        `Returing the list of cities of the uf [${uf}] from the cache`,
      );
      return this.formatHttpSuccessResponse(
        httpStatus.OK,
        JSON.parse(citiesFromCache),
      );
    }

    const data = await this.idebScrapperService.getAllCitiesUf(uf);
    this.cacheService.set(
      `${CacheKeys.CITIES}_${String(uf).toUpperCase()}`,
      JSON.stringify(data),
    );
    return this.formatHttpSuccessResponse(httpStatus.OK, data);
  }
}
