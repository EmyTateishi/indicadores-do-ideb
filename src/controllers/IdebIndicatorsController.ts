import httpStatus from 'http-status-codes';

import BaseController from '@src/abstracts/AbstractController';
import {
  ICacheService,
  IController,
  IHttpRequest,
  IHttpResponse,
} from '@src/interfaces';
import { CacheKeys } from '@src/interfaces/enums';
import IdebScrapperService from '@src/services/IdebScrapperService';
import { logger } from '@src/shared';

export default class IdebController
  extends BaseController
  implements IController {
  constructor(
    private idebScrapperService: IdebScrapperService,
    private cacheService: ICacheService,
  ) {
    super();
  }

  async handleRoute(httpRequest: IHttpRequest): Promise<IHttpResponse> {
    this.handleMissingParams(['city', 'uf'], httpRequest, 'query');

    const { city, uf, grade, rede, nocache } = httpRequest.query;
    logger.info(
      `Getting data with the params: city: ${city}, uf: ${uf}, grade: ${
        grade || ''
      }, rede: ${rede || ''}`,
    );
    const indicatorsCacheKey = this.normalizeCacheKey({
      city,
      uf,
      grade,
      rede,
    });

    const indicatorsFromCache = this.cacheService.get(indicatorsCacheKey);

    if (indicatorsFromCache && !nocache) {
      logger.info(
        `Returning data with the params: city: ${city}, uf: ${uf}, grade: ${
          grade || ''
        }, rede: ${rede || ''} from the cache`,
      );
      return this.formatHttpSuccessResponse(
        httpStatus.OK,
        JSON.parse(indicatorsFromCache),
      );
    }

    const data = await this.idebScrapperService.getIdebDataByUfAndCity({
      city,
      uf,
      grade,
      rede,
    });

    this.cacheService.set(indicatorsCacheKey, JSON.stringify(data));
    return this.formatHttpSuccessResponse(httpStatus.OK, data);
  }

  private normalizeCacheKey({
    city,
    uf,
    grade,
    rede,
  }: {
    city: string;
    uf: string;
    grade?: string;
    rede?: string;
  }): string {
    return `${CacheKeys.INDICATORS}_${uf.toUpperCase()}_${city.toUpperCase()}${
      grade ? `_${grade.toUpperCase()}` : ''
    }${rede ? `_${rede.toUpperCase()}` : ''}`;
  }
}
