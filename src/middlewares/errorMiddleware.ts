import { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status-codes';

import { InvalidParamError } from '@src/services/IdebScrapperService/errors';
import { MissingParamError } from '@src/shared/errors';

import logger from '@shared/logger';

function statusCodeAsText(status: number): string {
  return httpStatus.getStatusText(status);
}

// eslint-disable-next-line max-params
export default function (
  error: InvalidParamError | MissingParamError,
  _req: Request,
  res: Response,
  next: NextFunction,
): void {
  if (error) {
    if (
      error instanceof InvalidParamError ||
      error instanceof MissingParamError
    ) {
      logger.error(error.message);
      res
        .status(httpStatus.BAD_REQUEST)
        .json({ code: statusCodeAsText(error.status), message: error.message });
      return;
    }

    logger.error(error);
    const errorCode = httpStatus.INTERNAL_SERVER_ERROR;
    res.status(errorCode).json({
      code: statusCodeAsText(errorCode),
      message: 'Something went wrong',
    });
  }

  next();
}
