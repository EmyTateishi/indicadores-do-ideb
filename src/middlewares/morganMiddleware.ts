import morgan from 'morgan';

import logger from '@shared/logger';

export default morgan(
  (tokens, req, res) =>
    [
      tokens.method(req, res),
      tokens.url(req, res),
      'STATUS_CODE_RESPONSE:',
      tokens.status(req, res),
      'RESPONSE_LENGTH:',
      tokens.res(req, res, 'content-length'),
      '-',
      'RESPONSE_TIME:',
      tokens['response-time'](req, res),
      'ms',
    ].join(' '),
  {
    stream: {
      write(message: string): void {
        logger.info(message);
      },
    },
  },
);
