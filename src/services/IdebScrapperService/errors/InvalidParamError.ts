const internalMessage = 'Error on process with the params provided';

export default class InvalidParamError extends Error {
  constructor(message: string, public status: number) {
    super(`${internalMessage} - ${message}`);
    this.name = this.constructor.name;
    Error.captureStackTrace(this, this.constructor);
  }
}
