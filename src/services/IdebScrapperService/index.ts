import httpStatus from 'http-status-codes';
import puppeteer, { Browser, Page } from 'puppeteer';

import { logger } from '@shared/index';

import { IIdebScrapperConfig } from '@interfaces/index';

import { InvalidParamError } from './errors';
import {
  getCityOptionXpathSelector,
  getGradeOptionXpathSelector,
  getRedeOptionXpathSelector,
  getTabXpathSelector,
} from './helpers';
import { Elements, GradeNumberId, GradeTabId } from './interfaces/enums';
import IIdebNormalizedData from './interfaces/IIdebNormalizedData';
import IIdebNormalizedNationalData from './interfaces/IIdebNormalizedNationalData';
import IIdebNormalizedStateData from './interfaces/IIdebNormalizedStateData';
import ITableExtractedData from './interfaces/ITableExtractedData';
import ITableNationalExtractedData from './interfaces/ITableNationalExrtactedData';

export default class IdebScrapperService {
  private browser: Browser | null = null;

  private page: Page | null = null;

  constructor(private config: IIdebScrapperConfig) {}

  private async accessIdebPage(): Promise<void> {
    logger.info(`Entering on ${this.config.IDEB_URL}`);
    this.browser = await puppeteer.launch(this.config.browserConfig);
    this.page = await this.browser.newPage();
    await this.page.goto(this.config.IDEB_URL);
  }

  private async getStateOptionsFromThePage(
    statesOptionsSelector = Elements.SELECT_UF_FIELD_OPTION,
  ): Promise<string[]> {
    const states = await this.page?.$$eval(
      statesOptionsSelector,
      ([, ...elements]) => elements.map(element => element.textContent || ''),
    );

    return states || [];
  }

  private async getCityOptionsFromThePage(): Promise<string[]> {
    const cities = await this.page?.$$eval(
      Elements.SELECT_CITY_FIELD_OPTION,
      ([, ...elements]) => elements.map(element => element.textContent || ''),
    );

    return cities || [];
  }

  private async getRedeOptionsFromThePage(): Promise<string[]> {
    const redes = await this.page?.$$eval(
      Elements.SELECT_REDE_ENSINO_FIELD_OPTION,
      ([, ...elements]) => elements.map(element => element.textContent || ''),
    );

    return redes || [];
  }

  private async getGradeOptionsFromThePage(
    gradeFieldOptionSelector = Elements.SELECT_GRADE_FIELD_OPTION,
  ): Promise<string[]> {
    const grades = await this.page?.$$eval(
      gradeFieldOptionSelector,
      ([, ...elements]) => elements.map(element => element.textContent || ''),
    );

    return grades || [];
  }

  public async getIdebDataByCountry(
    esfera = 'municipal',
  ): Promise<IIdebNormalizedNationalData> {
    try {
      await this.accessIdebPage();
      await this.selectCountryRadioOption();
      await this.timeout(500);
      await this.clickOnSearchButton();
      await this.timeout(750);
      const quintoAnoData = await this.extractNationalTableData(0);
      await this.timeout(750);
      const nonoAnoData = await this.extractNationalTableData(1);
      await this.timeout(750);
      const terceiroAnoData = await this.extractNationalTableData(2);
      const result = this.normalizeNationalData(
        [
          quintoAnoData as ITableNationalExtractedData,
          nonoAnoData as ITableNationalExtractedData,
          terceiroAnoData as ITableNationalExtractedData,
        ],
        esfera as keyof Omit<ITableNationalExtractedData, 'ano'>,
      );

      await this.closeBrowser();
      return result;
    } catch (error) {
      await this.closeBrowser();
      throw error;
    }
  }

  public async getIdebDataByUf({
    uf,
    rede = 'Total',
    grade,
  }: {
    uf: string;
    rede?: string;
    grade?: string;
  }): Promise<IIdebNormalizedStateData> {
    try {
      await this.accessIdebPage();
      await this.selectUfRadioOption();
      await this.selectUfOption(
        uf,
        Elements.SELECT_UF_FIELD_STATES_FILTER,
        Elements.SELECT_UF_FIELD_OPTION_STATES_FILTER,
      );
      await this.selectRedeOption(rede);
      await this.selectGradeOption(
        grade,
        Elements.SELECT_GRADE_FIELD_STATE_FILTER,
        Elements.SELECT_GRADE_FIELD_OPTION_STATE_FILTER,
      );
      await this.clickOnSearchButton();
      const data = await this.getAllGradesData();
      await this.closeBrowser();
      return this.normalizeStateData(uf, rede as string, data);
    } catch (error) {
      await this.closeBrowser();
      throw error;
    }
  }

  public async getIdebDataByUfAndCity({
    uf,
    city,
    rede,
    grade,
  }: {
    uf: string;
    city: string;
    rede?: string;
    grade?: string;
  }): Promise<IIdebNormalizedData> {
    try {
      await this.accessIdebPage();
      await this.selectCityRadioOption();
      await this.selectUfOption(uf);
      await this.selectCityOption(city);
      await this.selectRedeOption(rede);
      await this.selectGradeOption(grade);
      await this.clickOnSearchButton();
      await this.timeout(500);
      const data = await this.getAllGradesData();
      await this.closeBrowser();
      return this.normalizeData(uf, city, data);
    } catch (error) {
      await this.closeBrowser();
      throw error;
    }
  }

  private async waitForElement(
    elementSelector: string,
    timeout = 3000,
  ): Promise<void> {
    logger.info(
      `Waiting for element [${elementSelector}] with timeout [${timeout}]`,
    );
    await this.page?.waitForSelector(elementSelector, {
      timeout,
    });
  }

  public async getAllUfData(): Promise<string[]> {
    try {
      await this.accessIdebPage();
      await this.selectCityRadioOption();
      await this.waitForElement(Elements.SELECT_UF_FIELD);
      const ufList = await this.getStateOptionsFromThePage();
      await this.closeBrowser();
      return ufList;
    } catch (error) {
      await this.closeBrowser();
      throw error;
    }
  }

  public async getAllCitiesUf(uf: string): Promise<string[]> {
    try {
      await this.accessIdebPage();
      await this.selectCityRadioOption();
      await this.waitForElement(Elements.SELECT_UF_FIELD);
      const ufList = await this.getStateOptionsFromThePage();
      if (!ufList.includes(uf.toUpperCase())) {
        throw new InvalidParamError(
          `The state provided [${uf}] does not exists on list [${ufList.join(
            ', ',
          )}]`,
          httpStatus.BAD_REQUEST,
        );
      }

      await this.selectUfOption(uf);
      await this.waitForElement(Elements.SELECT_CITY_FIELD_OPTION);
      const cityOptions = await this.getCityOptionsFromThePage();
      await this.closeBrowser();
      return cityOptions;
    } catch (error) {
      await this.closeBrowser();
      throw error;
    }
  }

  private async timeout(time: number): Promise<void> {
    logger.info(`Waiting for ${time}ms`);
    return new Promise(resolve => setTimeout(resolve, time));
  }

  private async selectCityRadioOption(): Promise<void> {
    const [, , municipioRadioButton] =
      (await this.page?.$$(Elements.REGION_RADIO_BUTTON)) || [];
    await municipioRadioButton?.click();
  }

  private async selectUfRadioOption(): Promise<void> {
    const [, estadoRadioOption] =
      (await this.page?.$$(Elements.REGION_RADIO_BUTTON)) || [];
    await estadoRadioOption?.click();
  }

  private async selectCountryRadioOption(): Promise<void> {
    const [brasilRadioOption] =
      (await this.page?.$$(Elements.REGION_RADIO_BUTTON)) || [];
    await brasilRadioOption?.click();
  }

  private normalizeStateData(
    uf: string,
    esfera: string,
    extractedData: ITableExtractedData,
  ): IIdebNormalizedStateData {
    logger.info(`Normalizing idep data from ${uf} - ${esfera}`);
    const info: IIdebNormalizedStateData = {
      estado: uf,
      esfera: esfera === 'Total' ? 'Todas as esferas' : esfera,
      indicadores: [
        {
          nome: 'Ideb Observado',
          anosEscolares: [],
        },
        {
          nome: 'Metas Projetadas',
          anosEscolares: [],
        },
      ],
    };

    Object.keys(extractedData).forEach(gradeDataKey => {
      if (!extractedData[gradeDataKey as keyof ITableExtractedData]?.length) {
        return;
      }

      info.indicadores[0].anosEscolares.push({
        numAnoEscolar: GradeNumberId[gradeDataKey as keyof ITableExtractedData],
        anos: [],
      });

      info.indicadores[1].anosEscolares.push({
        numAnoEscolar: GradeNumberId[gradeDataKey as keyof ITableExtractedData],
        anos: [],
      });

      const anosEscolaresIndex = info.indicadores[0].anosEscolares.findIndex(
        anoEscolar =>
          anoEscolar.numAnoEscolar ===
          GradeNumberId[gradeDataKey as keyof ITableExtractedData],
      );

      extractedData[gradeDataKey as keyof ITableExtractedData]?.forEach(
        ([year, result], index) => {
          if (index > 7) {
            info.indicadores[1].anosEscolares[anosEscolaresIndex].anos.push({
              ano: year,
              valor: result,
            });
          } else {
            info.indicadores[0].anosEscolares[anosEscolaresIndex].anos.push({
              ano: year,
              valor: result,
            });
          }
        },
      );
    });

    logger.info(`Idep data from ${uf} - ${esfera} normalized`);
    return info;
  }

  private normalizeNationalData(
    extractedDataList: ITableNationalExtractedData[],
    esfera: keyof Omit<ITableNationalExtractedData, 'ano'> = 'municipal',
  ): IIdebNormalizedNationalData {
    logger.info('Normalizing idep national data');
    const info: IIdebNormalizedNationalData = {
      esfera: esfera === 'total' ? 'todas as esferas de ensino' : esfera,
      indicadores: [
        {
          nome: 'Ideb Observado',
          anosEscolares: [],
        },
        {
          nome: 'Metas Projetadas',
          anosEscolares: [],
        },
      ],
    };

    extractedDataList.forEach(extractedData => {
      info.indicadores[0].anosEscolares.push({
        numAnoEscolar: extractedData.ano,
        anos: extractedData[esfera]
          .slice(0, 7)
          .map(([ano, valor]) => ({ ano, valor })),
      });

      info.indicadores[1].anosEscolares.push({
        numAnoEscolar: extractedData.ano,
        anos: extractedData[esfera]
          .slice(7)
          .map(([ano, valor]) => ({ ano, valor })),
      });
    });

    return info;
  }

  private normalizeData(
    uf: string,
    city: string,
    extractedData: ITableExtractedData,
  ): IIdebNormalizedData {
    logger.info(`Normalizing idep data from ${city}/${uf}`);
    const info: IIdebNormalizedData = {
      estado: uf,
      cidade: city,
      indicadores: [
        {
          nome: 'Ideb Observado',
          anosEscolares: [],
        },
        {
          nome: 'Metas Projetadas',
          anosEscolares: [],
        },
      ],
    };

    Object.keys(extractedData).forEach(gradeDataKey => {
      if (!extractedData[gradeDataKey as keyof ITableExtractedData]?.length) {
        return;
      }

      info.indicadores[0].anosEscolares.push({
        numAnoEscolar: GradeNumberId[gradeDataKey as keyof ITableExtractedData],
        anos: [],
      });
      info.indicadores[1].anosEscolares.push({
        numAnoEscolar: GradeNumberId[gradeDataKey as keyof ITableExtractedData],
        anos: [],
      });

      const anosEscolaresIndex = info.indicadores[0].anosEscolares.findIndex(
        anoEscolar =>
          anoEscolar.numAnoEscolar ===
          GradeNumberId[gradeDataKey as keyof ITableExtractedData],
      );

      extractedData[gradeDataKey as keyof ITableExtractedData]?.forEach(
        ([year, result], index) => {
          if (index > 7) {
            info.indicadores[1].anosEscolares[anosEscolaresIndex].anos.push({
              ano: year,
              valor: result,
            });
          } else {
            info.indicadores[0].anosEscolares[anosEscolaresIndex].anos.push({
              ano: year,
              valor: result,
            });
          }
        },
      );
    });

    logger.info(`Idep data from ${city}/${uf} normalized`);
    return info;
  }

  private async selectUfOption(
    uf: string,
    ufSelectElement = Elements.SELECT_UF_FIELD,
    ufOptionsSelectElement = Elements.SELECT_UF_FIELD_OPTION,
  ): Promise<void> {
    await this.waitForElement(ufSelectElement);
    const statesList = await this.getStateOptionsFromThePage(
      ufOptionsSelectElement,
    );
    logger.info(`Validating the provided uf [${uf}]`);
    if (!statesList.includes(uf)) {
      throw new InvalidParamError(
        `The state provided [${uf}] does not exists on list [${statesList.join(
          ', ',
        )}]`,
        httpStatus.BAD_REQUEST,
      );
    }

    logger.info(`Selecting state: ${uf}`);
    const selectUfField = await this.page?.$(ufSelectElement);
    await selectUfField?.click();
    await selectUfField?.type(uf);
    await this.page?.keyboard.press(String.fromCharCode(13));
  }

  private async getAllGradesData(): Promise<ITableExtractedData> {
    await this.timeout(750);
    const quintoAnoData = await this.extractTableData(GradeTabId.QUINTO_ANO);
    await this.timeout(500);
    await this.clickOnTabItem(GradeTabId.NONO_ANO);
    await this.timeout(500);
    const nonoAnoData = await this.extractTableData(GradeTabId.NONO_ANO);
    await this.timeout(500);
    await this.clickOnTabItem(GradeTabId.TERCEIRO_ANO_MEDIO);
    await this.timeout(500);
    const terceiroAnoMedioData = await this.extractTableData(
      GradeTabId.TERCEIRO_ANO_MEDIO,
    );
    return { quintoAnoData, nonoAnoData, terceiroAnoMedioData };
  }

  private async extractNationalTableData(
    tableIndex: number,
  ): Promise<ITableNationalExtractedData | undefined> {
    const table = (await this.page?.$$(Elements.TABLE_NATIONAL_RESULT)) || [];
    if (!table.length) {
      logger.warn('table with ideb indicators not found on page');
      return;
    }

    const tableRows = (await table[tableIndex]?.$$('tbody tr')) || [];

    if (!tableRows.length) {
      logger.warn('table with ideb indicators not found on page');
      return;
    }

    const [, ...years] =
      (await tableRows[2].$$eval('td', elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    const [, ...total] =
      (await tableRows[3].$$eval('td', elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    const [, ...estadual] =
      (await tableRows[5].$$eval('td', elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    if (tableIndex === 2) {
      const [, ...privada] =
        (await tableRows[6].$$eval('td', elements =>
          elements.map(element => Number(element.textContent)),
        )) || [];

      const [, ...publica] =
        (await tableRows[7].$$eval('td', elements =>
          elements.map(element => Number(element.textContent)),
        )) || [];

      const grades = [5, 9, 3];

      return {
        ano: grades[tableIndex],
        total: years.map((year, index) => [year, total[index]]),
        estadual: years.map((year, index) => [year, estadual[index]]),
        privada: years.map((year, index) => [year, privada[index]]),
        municipal: [],
        publica: years.map((year, index) => [year, publica[index]]),
      };
    }

    const [, ...municipal] =
      (await tableRows[6].$$eval('td', elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    const [, ...privada] =
      (await tableRows[7].$$eval('td', elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    const [, ...publica] =
      (await tableRows[8].$$eval('td', elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    const grades = [5, 9, 3];

    return {
      ano: grades[tableIndex],
      total: years.map((year, index) => [year, total[index]]),
      estadual: years.map((year, index) => [year, estadual[index]]),
      privada: years.map((year, index) => [year, privada[index]]),
      municipal: years.map((year, index) => [year, municipal[index]]),
      publica: years.map((year, index) => [year, publica[index]]),
    };
  }

  private async extractTableData(
    grade: string,
  ): Promise<[number, number][] | undefined> {
    logger.info(`Extracting data from ${grade}`);
    const [noResultsElement] =
      (await this.page?.$x(Elements.NO_RESULTS_SPAN)) || [];

    if (noResultsElement) {
      logger.warn(`No Data to extract from ${grade}`);
      return;
    }

    const [, ...years] =
      (await this.page?.$$eval(Elements.TABLE_YEAR_HEADERS, elements =>
        elements.map(element => Number(element.textContent)),
      )) || [];

    const [, ...results] =
      (await this.page?.$$eval(Elements.TABLE_RESULT_VALUES, elements =>
        elements.map(element =>
          element.textContent?.includes('*')
            ? 0
            : Number(element.textContent || 0),
        ),
      )) || [];

    return years.map((year, index) => [year, results[index]]);
  }

  private async clickOnTabItem(grade: string): Promise<void> {
    logger.info(`Clicking on tab ${grade}`);
    const [tabElement] =
      (await this.page?.$x(getTabXpathSelector(grade))) || [];
    await tabElement?.click();
  }

  private async selectCityOption(city: string): Promise<void> {
    const cityOptionXPathSelector = getCityOptionXpathSelector(
      city.toUpperCase(),
    );

    await this.waitForElement(Elements.SELECT_CITY_FIELD_OPTION);
    const cityOptions = await this.getCityOptionsFromThePage();
    logger.info(`Validating the provided city [${city}]`);
    if (!cityOptions.includes(city.toUpperCase())) {
      throw new InvalidParamError(
        `the city provided [${city}] does not exists on list, please check the list of cities to the uf provided`,
        httpStatus.BAD_REQUEST,
      );
    }
    logger.info(`Selecting city: ${city}`);
    const [cityOption] = (await this.page?.$x(cityOptionXPathSelector)) || [];

    await this.page?.evaluate(
      el => el.setAttribute('selected', 'true'),
      cityOption,
    );
  }

  private async selectRedeOption(rede = 'Municipal'): Promise<void> {
    await this.waitForElement(Elements.SELECT_REDE_ENSINO_FIELD_OPTION);
    const redeOptions = await this.getRedeOptionsFromThePage();
    logger.info(`Validating rede: ${rede}`);
    if (
      !redeOptions.map(item => item.toLowerCase()).includes(rede.toLowerCase())
    ) {
      throw new InvalidParamError(
        `The rede provided [${rede}] does not exists on list [${redeOptions.join(
          ', ',
        )}]`,
        httpStatus.BAD_REQUEST,
      );
    }

    logger.info(`Selecting rede: ${rede}`);

    const redeOptionSelector = getRedeOptionXpathSelector(rede);
    const [redeOption] = (await this.page?.$x(redeOptionSelector)) || [];
    await this.page?.evaluate(
      el => el.setAttribute('selected', 'true'),
      redeOption,
    );
  }

  private async selectGradeOption(
    grade = 'Todas',
    gradeSelectorElement = Elements.SELECT_GRADE_FIELD,
    gradeSelectorElementOption = Elements.SELECT_GRADE_FIELD_OPTION,
  ): Promise<void> {
    logger.info(`Validating grade: ${grade}`);
    const gradesList = await this.getGradeOptionsFromThePage(
      gradeSelectorElementOption,
    );
    if (
      !gradesList.map(item => item.toLowerCase()).includes(grade.toLowerCase())
    ) {
      throw new InvalidParamError(
        `The grade provided [${grade}] does not exists on list [${gradesList.join(
          ', ',
        )}]`,
        httpStatus.BAD_REQUEST,
      );
    }

    const gradeSelector = getGradeOptionXpathSelector(
      grade,
      gradeSelectorElement,
    );

    const [gradeOption] = (await this.page?.$x(gradeSelector)) || [];
    logger.info(`Selecting grade: ${grade}`);
    await this.page?.evaluate(
      el => el.setAttribute('selected', 'true'),
      gradeOption,
    );
  }

  private async clickOnSearchButton(): Promise<void> {
    logger.info('Clicking on search button');
    const searchButton = await this.page?.$(Elements.SEARCH_BUTTON);
    await searchButton?.click();
  }

  private async closeBrowser(): Promise<void> {
    logger.info('Closing Browser');
    await this.browser?.close();
  }
}
