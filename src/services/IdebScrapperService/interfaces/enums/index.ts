/* eslint-disable no-shadow */
export enum Elements {
  REGION_RADIO_BUTTON = 'input[name="regiaoDecorate:regiaoRadio"]',
  SELECT_UF_FIELD = 'select[name="uf2Decorate:uf2Select"]',
  SELECT_UF_FIELD_STATES_FILTER = 'select[name="ufDecorate:ufSelect"]',
  SELECT_UF_FIELD_OPTION = 'select[name="uf2Decorate:uf2Select"] option',
  SELECT_UF_FIELD_OPTION_STATES_FILTER = 'select[name="ufDecorate:ufSelect"] option',
  SELECT_CITY_FIELD = 'select[name="municipioDecorate:municipioSelect"]',
  SELECT_CITY_FIELD_OPTION = 'select[name="municipioDecorate:municipioSelect"] option',
  SELECT_REDE_ENSINO_FIELD = 'select[name="redeDependenciaDecorate:redeDependenciaSelect"]',
  SELECT_REDE_ENSINO_FIELD_OPTION = 'select[name="redeDependenciaDecorate:redeDependenciaSelect"] option',
  SELECT_GRADE_FIELD = 'select[name="serieAnoDecorate:serieAnoSelect"]',
  SELECT_GRADE_FIELD_STATE_FILTER = '*[@name="serieAnoEstadoDecorate:serieAnoEstadoSelect"]',
  SELECT_GRADE_FIELD_OPTION = 'select[name="serieAnoDecorate:serieAnoSelect"] option',
  SELECT_GRADE_FIELD_OPTION_STATE_FILTER = 'select[name="serieAnoEstadoDecorate:serieAnoEstadoSelect"] option',
  SEARCH_BUTTON = 'input[src="http://public.inep.gov.br/MECdefault/files/images/botoes/acao/vermelho/pesquisar.jpg"]',
  TABLE_YEAR_HEADERS = '.rich-table-subheader th',
  TABLE_RESULT_VALUES = '.rich-table-firstrow td',
  NO_RESULTS_SPAN = '//span[text()="Não existem resultados para a série informada."]',
  TABLE_NATIONAL_RESULT = '#TabelaHolder center table',
}

export enum GradeTabId {
  QUINTO_ANO = '4ª série / 5º ano',
  NONO_ANO = '8ª série / 9º ano',
  TERCEIRO_ANO_MEDIO = '3ª série EM',
}

export enum Rede {
  FEDERAL = 'Federal',
  ESTADUAL = 'Estadual',
  MUNICIPAL = 'Municipal',
  TODAS = 'Pública (Federal, Estadual e Municipal)',
}

export enum GradeNumberId {
  quintoAnoData = 5,
  nonoAnoData = 9,
  terceiroAnoMedioData = 3,
}
