interface IAnosEscolares {
  numAnoEscolar: number;
  anos: {
    ano: number;
    valor: number;
  }[];
}

export default interface IIdebNormalizedNationalData {
  esfera: string;
  indicadores: {
    nome: string;
    anosEscolares: IAnosEscolares[];
  }[];
}
