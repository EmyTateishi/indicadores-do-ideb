export default interface IIdebNormalizedData {
  estado: string;
  cidade: string;
  indicadores: {
    nome: string;
    anosEscolares: {
      numAnoEscolar: number;
      anos: {
        ano: number;
        valor: number;
      }[];
    }[];
  }[];
}
