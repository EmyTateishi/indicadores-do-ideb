export default interface IIdebNormalizedStateData {
  estado: string;
  esfera: string;
  indicadores: {
    nome: string;
    anosEscolares: {
      numAnoEscolar: number;
      anos: {
        ano: number;
        valor: number;
      }[];
    }[];
  }[];
}
