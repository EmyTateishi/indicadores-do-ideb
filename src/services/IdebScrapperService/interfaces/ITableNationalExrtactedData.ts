export default interface ITableNationalExtractedData {
  ano: number;
  total: [number, number][];
  estadual: [number, number][];
  privada: [number, number][];
  municipal: [number, number][];
  publica: [number, number][];
}
