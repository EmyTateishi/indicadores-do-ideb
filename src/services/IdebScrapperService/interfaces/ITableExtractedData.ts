export default interface ITableExtractedData {
  quintoAnoData?: [number, number][];
  nonoAnoData?: [number, number][];
  terceiroAnoMedioData?: [number, number][];
}
