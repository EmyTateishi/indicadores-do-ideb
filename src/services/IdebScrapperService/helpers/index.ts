import { Elements } from '../interfaces/enums';

export function getCityOptionXpathSelector(city: string): string {
  return `//select[@name="municipioDecorate:municipioSelect"]/option[text()="${city}"]`;
}

export function getRedeOptionXpathSelector(rede: string): string {
  return `//select[@name="redeDependenciaDecorate:redeDependenciaSelect"]/option[text()="${rede}"]`;
}

export function getGradeOptionXpathSelector(
  grade: string,
  gradeSelectorElement = Elements.SELECT_GRADE_FIELD,
): string {
  return `//${gradeSelectorElement}/option[text()="${grade}"]`;
}

export function getTabXpathSelector(grade: string): string {
  return `//td[text()="${grade}"]`;
}
