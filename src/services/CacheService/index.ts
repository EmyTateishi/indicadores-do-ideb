import NodeCache from 'node-cache';

import { ICacheService } from '@src/interfaces';

import { cacheConfig } from '@config/index';

export default class CacheService implements ICacheService {
  constructor(private cacheProvider = new NodeCache(cacheConfig)) {}

  set<T = any>(key: string, value: T): void {
    this.cacheProvider.set(key, value);
  }

  get<T = any>(key: string): T | undefined {
    return this.cacheProvider.get(key);
  }
}
