import { Response, Request } from 'express';

import { IController, IHttpRequest } from '@interfaces/index';

export default (
  controller: IController,
): ((req: Request, res: Response) => Promise<void>) => async (
  req: Request,
  res: Response,
) => {
  const httpRequest: IHttpRequest = {
    body: req.body,
    headers: req.headers,
    params: req.params,
    query: req.query,
  };

  const response = await controller.handleRoute(httpRequest);
  res.status(response.statusCode).json(response.body);
};
