import { IAppConfig } from '@interfaces/index';

const appConfig: IAppConfig = {
  PORT: Number(process.env.PORT) || 3333,
};

export default appConfig;
