import { Options } from 'node-cache';

const cacheConfig: Options = {
  stdTTL: Number(process.env.CACHE_TTL) || 3600,
  checkperiod: 120,
};

export default cacheConfig;
