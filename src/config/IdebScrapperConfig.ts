import { IIdebScrapperConfig } from '@interfaces/index';

const idepScrapperConfig: IIdebScrapperConfig = {
  IDEB_URL: String(process.env.IDEP_URL),
  browserConfig:
    process.env.NODE_ENV === 'production'
      ? {
          executablePath: process.env.CHROMIUM_PATH,
          headless: true,
          args: ['--disable-gpu', '--no-sandbox', '--disable-setuid-sandbox'],
        }
      : { headless: false, defaultViewport: null, args: ['--start-maximized'] },
};

export default idepScrapperConfig;
