import { ILoggerConfig } from '@interfaces/index';

const loggerConfig: ILoggerConfig = {
  enabled: !!process.env.ENABLE_LOG,
  level: process.env.LOG_LEVEL || 'info',
};

export default loggerConfig;
