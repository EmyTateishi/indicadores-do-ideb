export default interface IHttpRequest {
  headers?: any;
  params?: any;
  body?: any;
  query?: any;
}
