import { LaunchOptions } from 'puppeteer';

export default interface IdebScrapperConfig {
  IDEB_URL: string;
  browserConfig: LaunchOptions;
}
