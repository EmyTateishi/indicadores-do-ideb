// eslint-disable-next-line no-shadow
export enum ExitStatus {
  FAILURE = 1,
  SUCCESS = 0,
}

// eslint-disable-next-line no-shadow
export enum CacheKeys {
  STATES = 'IDEB_STATES',
  CITIES = 'IDEB_CITIES',
  INDICATORS = 'IDEB_INDICATORS',
}
