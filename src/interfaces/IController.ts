import { IHttpRequest, IHttpResponse } from '.';

export default interface IController {
  handleRoute(httpRequest: IHttpRequest): Promise<IHttpResponse>;
}
