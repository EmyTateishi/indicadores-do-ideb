export default interface ICacheService {
  set<T = any>(key: string, value: T): void;
  get<T = any>(key: string): T | undefined;
}
