import cors from 'cors';
import express, { Application } from 'express';
import 'express-async-errors';
import helmet from 'helmet';
import openapi from 'openapi-comment-parser';
import swaggerUI from 'swagger-ui-express';

import logger from '@shared/logger';

import routes from '@routes/index';

import { errorMiddleware, morganMiddleware } from './middlewares';
import openapiConfig from './openapirc';

const apiSchema = openapi(openapiConfig);

export default class App {
  private server: Application;

  constructor(private port: number) {
    this.server = express();
  }

  private setupGlobalMiddlewares() {
    logger.info('Setuping the global middlewares');
    this.server.use(morganMiddleware);
    this.server.use(helmet());
    this.server.use(cors());
  }

  private setupDocs(): void {
    logger.info('Setuping the api docs');
    this.server.use('/api/docs', swaggerUI.serve, swaggerUI.setup(apiSchema));
  }

  private setupGlobalErrorMiddleware() {
    logger.info('Setuping the global error middleware');
    this.server.use(errorMiddleware);
  }

  private setupRoutes() {
    logger.info('Setuping the application routes');
    this.server.use(routes);
  }

  public initApplication(): void {
    this.setupGlobalMiddlewares();
    this.setupDocs();
    this.setupRoutes();
    this.setupGlobalErrorMiddleware();
  }

  public initServer(): void {
    this.initApplication();
    this.server.listen(this.port, () =>
      logger.info(`Server listening on port ${this.port}`),
    );
  }

  public getServerInstance(): Application {
    return this.server;
  }
}
