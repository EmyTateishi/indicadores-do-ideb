import { Router } from 'express';

import expressRouterAdapter from '@src/adapters/expressRouterAdapter';
import { IdebStateIndicatorsController } from '@src/controllers';
import {
  idebStateIndicatorsControllerFactory,
  idebStatesControllerFactory,
  idebCitiesByUfControllerFactory,
  idebNationalControllerFactory,
} from '@src/factories';
import CacheService from '@src/services/CacheService';

const routes = Router();
const cacheService = new CacheService();

/**
 * GET /ideb
 * @tag Ideb
 * @summary Get the indicators by state/city from ideb page.
 * @description Get the indicators by state/city from ideb page.
 * @queryParam {boolean} [nocache] - to force the application scrape the info from the idep page.
 * @queryParam {UF} [uf] - The state where the indicators will be stracted.
 * @queryParam {string} [city] - The city where the indicators will be stracted.
 * @queryParam {GRADES} [grade] - The city where the indicators will be stracted (by default the endpoint search for all grades).
 * @queryParam {REDES} [rede] - the teaching network that the data will be stracted (by default the endpoint search only for Municipal teaching network).
 * @response 200 - The indicator based on provided params.
 * @responseContent {IndicadoresResponse} 200.application/json
 * @response 400 - Bad Request
 * @responseContent {BadRequestError} 400.application/json
 * @response 500 - Internal Server Error
 * @responseContent {InternalServerError} 500.application/json
 */
routes.get(
  '/api/ideb',
  expressRouterAdapter(idebStateIndicatorsControllerFactory(cacheService)),
);

/**
 * GET /ideb/bystates
 * @tag Ideb
 * @summary Get the indicators by state from ideb page.
 * @description Get the indicators by state from ideb page.
 * @queryParam {boolean} [nocache] - to force the application scrape the info from the idep page.
 * @queryParam {UF} [uf] - The state where the indicators will be stracted.
 * @queryParam {GRADES} [grade] - The city where the indicators will be stracted (by default the endpoint search for all grades).
 * @queryParam {REDES_POR_ESTADO} [rede] - the teaching network that the data will be stracted (by default the endpoint search only for Municipal teaching network).
 * @response 200 - The indicator based on provided params.
 * @responseContent {IndicadoresStatesResponse} 200.application/json
 * @response 400 - Bad Request
 * @responseContent {BadRequestError} 400.application/json
 * @response 500 - Internal Server Error
 * @responseContent {InternalServerError} 500.application/json
 */
routes.get(
  '/api/ideb/bystates',
  expressRouterAdapter(idebStateIndicatorsControllerFactory(cacheService)),
);

/**
 * GET /ideb/bycountry
 * @tag Ideb
 * @summary Get the indicators from all country from ideb page.
 * @description Get the indicators from all country.
 * @queryParam {boolean} [nocache] - to force the application scrape the info from the idep page.
 * @queryParam {REDES_PAIS} [rede] - the teaching network that the data will be stracted (by default the endpoint search only for Municipal teaching network).
 * @response 200 - The indicator based on provided params.
 * @responseContent {IndicadoresCountryResponse} 200.application/json
 * @response 400 - Bad Request
 * @responseContent {BadRequestError} 400.application/json
 * @response 500 - Internal Server Error
 * @responseContent {InternalServerError} 500.application/json
 */
routes.get(
  '/api/ideb/bycountry',
  expressRouterAdapter(idebNationalControllerFactory(cacheService)),
);

/**
 * GET /ideb/states
 * @tag Ideb
 * @summary Get the states that exists on idep page.
 * @queryParam {boolean} [nocache] - to force the application scrape the info from the idep page.
 * @description Get the state options that exists on idep page.
 * @response 200 - The list of states
 * @responseContent {States} 200.application/json
 * @response 400 - Bad Request
 * @responseContent {BadRequestError} 400.application/json
 * @response 500 - Internal Server Error
 * @responseContent {InternalServerError} 500.application/json
 */
routes.get(
  '/api/ideb/states',
  expressRouterAdapter(idebStatesControllerFactory(cacheService)),
);

/**
 * GET /ideb/cities/{uf}
 * @tag Ideb
 * @summary Get the cities by thet provided state.
 * @queryParam {boolean} [nocache] - to force the application scrape the info from the idep page.
 * @pathParam {UF} [uf] - state that the application will get the cities.
 * @description Get the list of cities by the provided state.
 * @response 200 - The list of cities
 * @responseContent {Cities} 200.application/json
 * @response 400 - Bad Request
 * @responseContent {BadRequestError} 400.application/json
 * @response 500 - Internal Server Error
 * @responseContent {InternalServerError} 500.application/json
 */
routes.get(
  '/api/ideb/cities/:uf',
  expressRouterAdapter(idebCitiesByUfControllerFactory(cacheService)),
);

export default routes;
