const { resolve } = require('path');

const rootDir = resolve(__dirname);

module.exports = {
  rootDir,
  displayName: 'root-tests',
  clearMocks: true,
  collectCoverage: true,
  collectCoverageFrom: ['./src/**'],
  coverageDirectory: 'coverage',
  coveragePathIgnorePatterns: [
    '\\\\node_modules\\\\',
    'src/index.ts',
    '__tests__',
    'src/@types',
    'src/interfaces',
    'src/api.schema.json',
  ],
  coverageProvider: 'v8',
  preset: 'ts-jest',
  moduleNameMapper: {
    '@src/(.*)': '<rootDir>/src/$1',
    '@tests/(.*)': '<rootDir>/__tests__/$1',
    '@shared/(.*)': '<rootDir>/shared/$1',
    '@config/(.*)': '<rootDir>/config/$1',
    '@services/(.*)': '<rootDir>/services/$1',
    '@controllers/(.*)': '<rootDir>/controllers/$1',
    '@middlewares/(.*)': '<rootDir>/middlewares/$1',
    '@routes/(.*)': '<rootDir>/routes/$1',
    '@interfaces/(.*)': '<rootDir>/interfaces/$1',
    '@adapters/(.*)': '<rootDir>/adapters/$1',
  },
  testEnvironment: 'node',
  testMatch: ['<rootDir>/__tests__/*.spec.ts'],
  testPathIgnorePatterns: ['\\\\node_modules\\\\'],
};
