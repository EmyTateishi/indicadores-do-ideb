FROM alpine:latest

WORKDIR /app

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories \
    && apk upgrade -U -a \
    && apk add \
    libstdc++ \
    chromium \
    harfbuzz \
    nss \
    freetype \
    ttf-freefont \
    font-noto-emoji \
    wqy-zenhei \
    nodejs \
    nodejs-npm \
    && rm -rf /var/cache/* \
    && mkdir /var/cache/apk

COPY . /app/

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

RUN npm i --ignore-scripts
RUN npm run build
RUN cp src/api.schema.yaml dist/src/api.schema.yaml

ENTRYPOINT ["node", "dist/src/index.js"]
