# Indicadores da educação

## deploy no heroku

1. logar no registry

```bash
heroku container:login
```

2. criar as variaveis de ambiente no painel do heroku conforme o arquivo `.env.example`

informar as credenciais para autenticar

3. subir a imagem para o registry do heroku

```bash
heroku container:push web --app nomeDoAppNoHeroku
```

4. subir a aplicação

```bash
heroku container:release web --app nomeDoAppNoHeroku
```

5. ver os logs da aplicação

```bash
heroku logs --tail --app nomeDoAppNoHeroku
```

Obs: os logs da aplicação estão bem intuitivos descrevendo passo a passo o que está ocorrendo na aplicação, bem como
os eventuais erros que podem acontecer, segue exemplo abaixo.

![Exemplo de logs](2020-12-05-18-46-36.png)

## executando localmente

1. instalar as dependencias

```bash
yarn install

# ou

npm install
```

2. criar os arquivo na raiz do projeto .env com as variaveis de ambiente conforme o arquivo .env.example

3. executar o comando abaixo:

```bash
npm run start:dev

# ou

yarn start:dev
```

4. acessar a seguinte [url](http://localhost:3000/api/docs/)

nela estão contidas as rotas para consultar os dados da aplicação bem como os parametros que devem ser passados.

## Executando com docker

1. executar o comando abaixo

```bash
docker-compose up
```

o comando irá construir a imagem e subir a mesma com as variaveis de ambiente necessarias já definidas.

2. acessar a seguinte [url](http://localhost:3000/api/docs/)

nela estão contidas as rotas para consultar os dados da aplicação bem como os parametros que devem ser passados.

