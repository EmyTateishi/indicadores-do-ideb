# 1.0.0 (2020-12-07)


### build

* add commitlint config to validate the commit messages on project ([de5279e](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/de5279e88591aeaa10301e4275bdc1d40195005e))
* add dependencies of puppeteer and config the tsconfig build ([ac76120](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/ac76120f94c6a6b95b487c13839d2a23e0190067))
* add docker-compose to make easier to run the project ([99be63b](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/99be63b3729306f2ada905af98a4d40c6300f812))
* add dockerfile to deploy the application ([06b35a0](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/06b35a04017e64730469fdaa57013aca925fe36e))
* add git commitzen to make easier follow the conventional commit ([e211bf6](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/e211bf62266ff0600c74dfaa21cee518063e6603))
* add husky and lint staged config ([10e21bb](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/10e21bb7d9d6cf814280bc0a0efe1dd263fa079c))
* add the command to lint-staged to run tests of the staged files ([e69885d](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/e69885dbc39d32052dc9166009e70561732e04d2))
* adjuste the docker-compose to build the container on run ([7b7b4e2](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/7b7b4e2caba3d0c44a001dbd552edb2f4ef7c69d))
* fix the config of jest to run the tests correctally ([a9ca3a1](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/a9ca3a161f7f4b700830635f50f9a78f3879d531))

### chore

* add commitzen deps ([93cab38](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/93cab383e5f46bbbe3d7c45ef62155b4deebb5bc))

### docs

* add the docs of how to run the project ([febb2e6](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/febb2e6abff91f0515054b8264c91955ae30e844))
* add the swagger documentation ([81f3ca1](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/81f3ca1ef32c5423faf5a6a5c513f1c77304507b))
* change the default server to use production ([3c1cde1](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/3c1cde184fb51f0706cd1ce866063b2df128ea06))

### feat

* add cache to all the routes ([35a27bf](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/35a27bf4ef9b512ce23d6977d7c73996f1156e7e))
* add global error middleware to capture errors on routes ([b4558c2](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/b4558c2f83ecd803ce705b74688201d274e19e27))
* add helmet to security middleware and cors to enable external requests ([67c7871](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/67c7871e8c53fb2c96f2e0c4b3593bdb765e96e1))
* add jest config to create the tests on app ([71a678b](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/71a678b9b1a3bb8eee55a8f047ff8567720f5f99))
* add morgan middleware to log all the request details ([ea4cfa5](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/ea4cfa57aad477a5e243baae3817cc92462d6d7b))
* add project startup process ([ac7cb35](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/ac7cb35b70560569050e0f8fa66dd335a5c4a377))
* add the cities by state route to get the cities from the page ([abea325](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/abea32559d4ab34735c8387d5f3a532ce26ffe72))
* add the controller that provide the indicators data by uf/city ([25ddb1b](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/25ddb1b3471248c25e6a6c27762975bc57c4cc0f))
* add the eslint and prettier config ([ade5e01](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/ade5e016d0f43589f1b64b943711fb9aa7bea11a))
* add the routes to get all the states on ideb page ([9404a7c](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/9404a7c7cdf74abc05ca6ba28fa56f1543efa874))
* add the scraper service to get the ideb data ([dd49b40](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/dd49b400380e5501fc213b6e397782b3f3d8b1c1))
* create base project stratucture ([84877ae](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/84877ae0482be0306552200d393054bd97cdaa2d))
* create the idep scraper service to get data from the idep site ([d40fc84](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/d40fc84f98dabf61af55d57d1f7c770c0076cf3d))

### fix

* add a timeout to wait the data load before extract ([6b865bf](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/6b865bff603ea1582a7affc7a6e5f85b22bdf29c))
* add an waitForElement to avoid bugs ([f595b7c](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/f595b7c5e1b39e2bbd9741d9e2a4574faa25027c))
* add handler asterisks on result values ([9ae57d5](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/9ae57d5af5084ad4721dbcb72fd7b02c50cc66a9))

### refactor

* remove the not required command of lint-staged ([98ca2dd](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/98ca2dd9c54aa7f8aea84f494ad469d18fe8b320))

### style

* add editorconfig to follow th code style guide on all IDE's ([18a929b](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/18a929b3c4d392ef7c22b9a5e07c69430853489f))
* add interfaces imports to import config ([95e33e0](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/95e33e06c25e7848e70d3307beee900ced1f90e6))
* add the paths config to make easier the file import on project ([41a395f](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/41a395fdf3862a78aa886231f3e0528e4a03d95d))
* add the paths to import helpers config to order the file imports ([6d91666](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/6d9166646c698cd0a994d569ddcf1f24814ba2ae))
* create eslintignore file to ignore the files we want ([1a092ac](https://gitlab.com/EmyTateishi/indicadores-do-ideb/commit/1a092ac2fd09bf1baf5958bf00eeb0243b2f4efa))


